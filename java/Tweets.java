import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import com.mongodb.util.JSON;

public class Tweets {

	private String result="";
	private HttpGet request = null;
	private int number = 0;

	//MongoDB connection
	private DB db = null;

	//MongoDB server
	private Mongo m = null;

	/**
	 * Constructeur de Tweets à un argument.
	 * Cas 'save' et 'search'
	 * @param arg : Commande provenant du client
	 */
	public Tweets(String arg) {
	
		System.out.println(arg);
		Authentication app = getConnectionData("app.xml");
		connect(); //bdd
		request = connect(app); //Twitter
		if(arg.equals("save"))
			retrieve(request); //Récup et Sauvegarde
		else if(arg.equals("search"))
			search(null);
		else
			System.out.println("Argument invalide.");
		
	}

	/**
	 * Constructeur de Tweets à 2 arguments.
	 * Cas 'search <user>', 'tweets <user>' et 'followers <user>'
	 * @param arg
	 * @param arg2
	 */
	public Tweets(String arg, String arg2) {
		System.out.println(arg);
		System.out.println(arg2);
		Authentication app = getConnectionData("app.xml");
		connect(); //bdd
		
		if(arg.equals("search"))
		{
			search(arg2);
		}
		else if(arg.equals("tweets"))
		{
			request = connectUser(app,arg2);
			personTweets(request);
		}
		else if (arg.equals("followers"))
		{
			request = connectFollowers(app,arg2);
			followers(request);
		}
		else
			System.out.println("Arguments invalides.");
	}

	/**
	 * Méthode récupérant les followers. Les affiche et stocke le résultat
	 * @param arg2 : Connexion
	 */
	private void followers(HttpGet arg2) {
		
		String in;
		try {

			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			if (entity != null) {

				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));

				in = reader.readLine();
			    System.out.println(in);
			    
    			DBObject object = (DBObject)(JSON.parse(in));
    			BasicDBList listusers = (BasicDBList)object.get("users");
    			
    			for (int i = 0; i < listusers.size(); i++) {
			    	BasicDBObject anUser = (BasicDBObject)listusers.get(i);
			    	System.out.println(anUser.get("screen_name"));
			    	result+=anUser.get("screen_name");
			    	result+='\n';
			    }
			    
			} // if

		} // try
		catch (Exception e) {

			e.printStackTrace();

		} // catch

	}

	/**
	 * Méthode récupérant les tweets. Les affiche et stocke le résultat
	 * @param arg2 : Connexion
	 */
	private void personTweets(HttpGet arg2) 
	{
		String in;
		try {

			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			if (entity != null) {

				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));

				in = reader.readLine();
			    System.out.println(in);
			    BasicDBList list = (BasicDBList)JSON.parse(in);
			    for (int i = 0; i < list.size(); i++) {
			    	BasicDBObject aTweet = (BasicDBObject)list.get(i);
			    	System.out.println(aTweet.get("text"));
			    	result+=aTweet.get("text");
			    	result+='\n';
			    	result+='\n';
			    }
			} // if

		} // try
		catch (Exception e) {

			e.printStackTrace();

		} // catch
	}

	/**
	 * Méthode récupérant des tweets enregistré dans la base selon un terme de recherche.
	 * Si terme = null, "cute" sera utilisé comme terme.
	 * Les affiche et les enregistre.
	 * @param term : Terme de reherche
	 */
	private void search(String term) {
		
		DBCollection coll = db.getCollection(Configuration.mongo_collection);
		
		String regex = ".*";
		if(term==null) term="cute";
		regex += term;
		regex += ".*"; 
		
		 DBObject query =
				 QueryBuilder.start("text").regex(Pattern.compile(regex)).get();
		 
		 System.out.println(query);
		 
		 System.out.println("\nRésultat:");
		 DBCursor cursor = coll.find(query);
		 if(cursor.count() == 0) {
			 System.out.println("<vide>");
		 } else {
			 while(cursor.hasNext()) {
				 BasicDBObject t = (BasicDBObject)cursor.next();
				 System.out.println(t.get("screen_name")); 
				 System.out.println(t.get("text"));
				 
				 
				 result+=t.get("screen_name");
				 result+='\n';
				 result+=t.get("text");
				 result+='\n';
		 	}
		 }
		 cursor.close(); 
		
		//while(cursor.hasNext())
		//{
		//	System.out.println(cursor.next().get("text"));
		//}
		
	}

	/**
	 * The connection method connects to the MongoDB database
	 */
	private void connect() {
		
		try {
			
			m = new Mongo( Configuration.mongo_location , Configuration.mongo_port );
			db = m.getDB(Configuration.mongo_database);
			
		} //try
		catch (UnknownHostException | MongoException e) {
			
			e.printStackTrace();
			
		} //catch		
		
	} //connect
	
	/**
	 * The retrieve method is responsible to read the content from the stream
	 * and to provide to consumers
	 * 
	 * @param request
	 *            the request to access the stream
	 */
	private void retrieve(HttpGet request) {

		String in;
		try {

			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			if (entity != null) {

				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));

				// since this is a continuous stream, there is no end with the
				// readLine, we just check whether live boolean variable is set
				// to false
				boolean end=false;
			    while ((in = reader.readLine()) != null && end!=true) {
			    	if (!in.startsWith("{\"delete"))
			    	{
			    			System.out.println(in);
			    			DBObject object = (DBObject)(JSON.parse(in));
			    			BasicDBObject user = (BasicDBObject)object.get("user");
			    			BasicDBObject data = new BasicDBObject();
			    			
			    			data.put("screen_name", user.get("screen_name"));
			    			data.put("text",object.get("text"));
			    			data.put("created_at",object.get("created_at"));
			    			data.put("entities",object.get("entities"));
			    			System.out.println(data);
			    			DBCollection collection = db.getCollection(Configuration.mongo_collection);
			            	collection.insert(object);
			            	number++;
			    	}
			    	if(number == 500)
			    		end=true;
			     
			    } // while



			} // if

		} // try
		catch (Exception e) {

			e.printStackTrace();

		} // catch

	} // retrieve()
	
	/**
	 * getConnectionData retrieves the data for the authentication
	 * @param stFile the file containing the data for the OAuth authentication
	 * @return an instance of Authentication containing the data
	 */
	private Authentication getConnectionData(String stFile) {
	
		Authentication sr = null;
		
		try {
			
			//open the file and parse it to retrieve the four required information
			File file = new File(stFile);
			InputStream inputStream;
			inputStream = new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream, "ISO-8859-1");
	
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			
			XMLReader saxReader = XMLReaderFactory.createXMLReader();
			sr = new Authentication();
			saxReader.setContentHandler(sr);
			saxReader.parse(is);
			
		} //try
		catch (FileNotFoundException e) {

			e.printStackTrace();

		} // catch
		
		catch (UnsupportedEncodingException e) {

			e.printStackTrace();

		} // catch
		
		catch (SAXException e) {

			e.printStackTrace();

		} // catch
		
		catch (IOException e) {

			e.printStackTrace();

		} // catch

		return sr;
		
	} //getConnectionData()
	
	/**
	 * The connect method connects to the stream via OAuth
	 * 
	 * @param app
	 *            the data for connection
	 * @return the request
	 */
	private HttpGet connect(Authentication app) {

		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				app.getConsumerKey(), app.getConsumerSecret());

		consumer.setTokenWithSecret(app.getAccessToken(), app.getAccessSecret());
		
		HttpGet request = new HttpGet(Configuration.oauthstream);

		//HttpGet request = new HttpGet("https://userstream.twitter.com/1.1/user.json?replies=all");
		try {

			consumer.sign(request);

		} // try
		catch (OAuthMessageSignerException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthExpectationFailedException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthCommunicationException e) {

			e.printStackTrace();

		} // catch

		return request;

	} // connect()
	
	/**
	 * Methode de connection à l'api Twitter pour les derniers tweets d'un utilisateur
	 * @param app
	 * @param user : L'utilisateur duquel récupérer les tweets
	 * @return the request
	 */
	private HttpGet connectUser(Authentication app, String user) {

		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				app.getConsumerKey(), app.getConsumerSecret());

		consumer.setTokenWithSecret(app.getAccessToken(), app.getAccessSecret());
		
		HttpGet request = new HttpGet("https://api.twitter.com/1.1/statuses/user_timeline.json?trim_user=t&screen_name=" + user);

		try {

			consumer.sign(request);

		} // try
		catch (OAuthMessageSignerException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthExpectationFailedException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthCommunicationException e) {

			e.printStackTrace();

		} // catch

		return request;

	} // connect()
	
	/**
	 * Methode de connection à l'api Twitter pour les followers d'un utilisateur.
	 * Récupère les 50 derniers
	 * @param app
	 * @param user : L'utilisateur duquel récupérer les followers
	 * @return the request
	 */
	private HttpGet connectFollowers(Authentication app, String user) {

		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
				app.getConsumerKey(), app.getConsumerSecret());

		consumer.setTokenWithSecret(app.getAccessToken(), app.getAccessSecret());
		
		HttpGet request = new HttpGet("https://api.twitter.com/1.1/followers/list.json?skip_status=t&include_user_entities=false&count=50&screen_name=" + user);

		try {

			consumer.sign(request);

		} // try
		catch (OAuthMessageSignerException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthExpectationFailedException e) {

			e.printStackTrace();

		} // catch
		catch (OAuthCommunicationException e) {

			e.printStackTrace();

		} // catch

		return request;

	} // connect()
	
	public static void main(String[] args) throws IOException
	{
		if (args.length != 1) {
	        System.err.println("Usage: java Tweets <port number>");
	        System.exit(1);
	    }

	        int portNumber = Integer.parseInt(args[0]);
	        boolean listening = true;
	        
	        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
	        	System.out.println("Serveur lancé sur le port " + portNumber);
	            while (listening) {
		            new ServerThread(serverSocket.accept()).start();
		            System.out.println("Requête reçue");
		        }
		    } catch (IOException e) {
	            System.err.println("Could not listen on port " + portNumber);
	            System.exit(-1);
	        }
		
		
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
