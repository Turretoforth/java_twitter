import java.net.*;
import java.io.*;

public class ServerThread extends Thread {
	private Socket socket = null;

	public ServerThread(Socket socket) {
		super("ServerThread");
		this.socket = socket;
	}

	public void run() {

		try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));) {
			String input, output;

			input = in.readLine();
			output = process(input);
			out.println(output);
			out.println("Bye.");
			System.out.println("Arr�t du thread");
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String process(String input) {
		String output;
		System.out.println("Re�u : " + input);

		if (input.equalsIgnoreCase("search")) {
			Tweets t = new Tweets("search");
			output = t.getResult();
		} else if (input.matches("save")) {
			new Tweets("save");
			output = "Enregistrement de 500 tweets effectu�";
		} else if (input.matches("search .*")) {
			Tweets t = new Tweets("search", input.substring(7));
			output = t.getResult();
		} else if (input.matches("tweets .*")) {
			Tweets t = new Tweets("tweets", input.substring(7));
			output = t.getResult();
		} else if (input.matches("followers .*")) {
			Tweets t = new Tweets("followers", input.substring(10));
			output = t.getResult();
		}
		else {
			System.out.println("Input invalide");
			output = "error";
		}
		return output;
	}

}
